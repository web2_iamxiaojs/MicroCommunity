package com.java110.config.properties.code;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by wuxw on 2017/7/25.
 */
@Component
@ConfigurationProperties(prefix = "java110")
@PropertySource("classpath:java110.properties")
public class Java110Properties {

    private String mappingPath;

    private String wxAppId;

    private String wxAppSecret;

    private boolean autoReloadComponent = false;

    public String getMappingPath() {
        return mappingPath;
    }

    public void setMappingPath(String mappingPath) {
        this.mappingPath = mappingPath;
    }

    public String getWxAppId() {
        return wxAppId;
    }

    public void setWxAppId(String wxAppId) {
        this.wxAppId = wxAppId;
    }

    public String getWxAppSecret() {
        return wxAppSecret;
    }

    public void setWxAppSecret(String wxAppSecret) {
        this.wxAppSecret = wxAppSecret;
    }

    public boolean getAutoReloadComponent() {
        return autoReloadComponent;
    }

    public void setAutoReloadComponent(boolean autoReloadComponent) {
        this.autoReloadComponent = autoReloadComponent;
    }
}
